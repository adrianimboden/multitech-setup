# Multitech Setup

A collection of scripts to be downloaded on to a Multitech gateway for configuration on the Measuremen network

# How to setup

Before a gateway can be joined to the network, it must be running the correct firmware and be connected to the internet. These steps must be carried out befor the automated script can take over.

## 1. Set up host computer

When a gateway comes fresh out of the box, it must first be turned on and an ethernet cable run direct from the target computer to the device.

Set ethernet network settings to:
- Static IP
- IP Address: 192.168.2.2
- Netmask: 255.255.255.0
- Gateway: empty (if required, add 192.168.2.1)

The gateway should now be accessible via its default IP address: 192.168.2.1

## 2. First connection to gateway

Connecting to the gateway varies depending on which firmware version it came with out of the box. As this is impossible to tell from first glance, try both methods and see which one works.

### Commissioning procedure (firmware >=5.2)
1. In your browser, navigate to https://192.168.2.1/
3. Set username and password

It's recommended to use username `mtadm` and a company default password.

### Default username/password (firmware <5.2)
Connect to the device over SSH, with IP address 192.168.2.1.

Assume the default user/password is either:
- username: `mtadm` password: `root`
- username: `root` password: `root`

### SSH Connection
Using an SSH client, connect to `192.168.2.1`. If you're connecting using an account that is not `root`, ensure any commands you run after this point use `sudo`. ProTip: use `sudo -i` to open a root shell, so you don't need to add sudo to every subsequent command.

## 3. Establish internet connection

Once connected directly to the device, run `sudo -i`, enter the password you gave above, then copy/paste the following commands:

```
sed -i 's/eth0 inet static$/eth0 inet dhcp/g' /etc/network/interfaces
sed -i 's/^address/# address/g; s/^netmask/# netmask/g' /etc/network/interfaces
reboot
```
This will set the network connection to use DHCP, and reboot. Now unplug the device from your computer, and connect the device to the network.
You will need to find the device on the network to obtain its IP address. Try checking the router interface for its MAC address, or on Linux type `nmap x.x.x.x/x -p 22 --open` where x is the IP address and subnet mask.

## 4. Upgrade firmware

Run these commands to upgrade firmware:

```
curl https://www.multitech.net/mlinux/images/mtcap/5.2.7/mlinux-mtcap-commissioning-image-mtcap-upgrade-withboot.bin > /var/volatile/upgrade.bin
mlinux-firmware-upgrade /var/volatile/upgrade.bin
```

This should download the latest tested firmware from Multitech, and reboot & reset the device. Follow steps 1-3 again before continuing, and use the commissioning procedure.

## 5. Run script

From here the automated script can take over. Run this command:

```
# Run this first, then enter password
sudo -i
# Copy/paste all the below lines into the console at once and hit enter
curl --fail https://gitlab.com/measuremen/multitech-setup/-/raw/master/setup.sh | \
ORG_NAME=<organisation name as shown in LNS> \
LNS_SERVER=<domain name for accessing the TTIv3 instance> \
API_KEY=<An Organisation API key with full gateway permissions> \
bash
```

Make sure all 3 secret environment variables are set first before executing the script.

- ORG_NAME: The id of the organization within the TTIv3 instance
- LNS_SERVER: the address of the instance, structured as `<name>.<region>.cloud.thethings.industries`
- API_KEY: Go to your organization and generate an API key with gateway edit/create/apikey previleges. Paste the secret key here.
