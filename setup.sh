HOMEDIR=/opt/lora
PRODUCT_ID=$(mts-io-sysfs show lora/product-id | tr \"[:upper:]\" \"[:lower:]\")
DEVICE_ID=$(mts-io-sysfs show device-id)
GATEWAY_EUI=$(mts-io-sysfs show lora/eui | sed s/://g)

if [[ -v ORG_NAME ]] && [[ -v LNS_SERVER ]] && [[ -v API_KEY ]];
then
  echo "Secret variables have been set"
else
  echo "ERROR: secrets have not been set. Make sure ORG_NAME, LNS_SERVER and API_KEY have been set."
  exit 1
fi

echo "Setting hostname"
echo "$PRODUCT_ID-$DEVICE_ID" > /etc/hostname

echo "Setting up time sync"
ln -fs /usr/share/zoneinfo/UTC /etc/localtime
cat > /etc/default/ntpdate <<EOF
NTPSERVERS="europe.pool.ntp.org"
UPDATE_HWCLOCK="yes"
EOF
ntpdate-sync

echo "Installing basicstation"
curl https://www.multitech.net/downloads/{mbedtls_2.13.0-r0.0,lora-basic-station_2.0.5-r1.0}_arm926ejste.ipk -o /var/volatile/mbedtls.ipk -o /var/volatile/lora-basic-station.ipk
opkg update && opkg install monit
opkg install /var/volatile/mbedtls.ipk
opkg install /var/volatile/lora-basic-station.ipk
opkg remove lora-network-server
opkg remove lora-packet-forwarder

echo "Configuring basicstation SSL"
echo wss://$LNS_SERVER:8887 > $HOMEDIR/tc.uri
curl https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem.txt > $HOMEDIR/tc.trust

echo "Creating gateway $PRODUCT_ID-$DEVICE_ID in $LNS_SERVER/$ORG_NAME organization"
curl https://gitlab.com/measuremen/multitech-setup/-/raw/master/gateway.json | sed "s/{PRODUCT_ID}/$PRODUCT_ID/g; s/{DEVICE_ID}/$DEVICE_ID/g; s/{GATEWAY_EUI}/$GATEWAY_EUI/g; s/{LNS_SERVER}/$LNS_SERVER/g" > /var/volatile/gateway.json
curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $API_KEY" -d @/var/volatile/gateway.json https://$LNS_SERVER/api/v3/organizations/$ORG_NAME/gateways

echo "Creating Gateway access key and writing to tc.key"
curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $API_KEY" -d '{"gateway_ids":{},"name":"basicstation","rights":["RIGHT_GATEWAY_LINK"]}' https://$LNS_SERVER/api/v3/gateways/$PRODUCT_ID-$DEVICE_ID/api-keys | \
grep -Eo 'NNSXS\.[A-Z0-9]{39}\.[A-Z0-9]{52}' | sed 's/^/Authorization: /; s/$/\r/' > $HOMEDIR/tc.key

echo "Downloading & configuring station.conf"
curl https://gitlab.com/measuremen/multitech-setup/-/raw/master/station.conf | sed "s/{ROUTERID}/$GATEWAY_EUI/g" > $HOMEDIR/station.conf

echo "Downloading init file"
curl https://gitlab.com/measuremen/multitech-setup/-/raw/master/station.sh > /etc/init.d/station.sh && chmod +x /etc/init.d/station.sh

echo "Configuring monit connection monitoring"
sed -i 's/="no"/="yes"/' /etc/default/monit
cat > /etc/monit.d/internet <<EOF
check host internet with address 1.1.1.1
  program start = "/etc/init.d/ppp start" with timeout 60 seconds
  program stop = "/etc/init.d/ppp stop" with timeout 60 seconds
  if failed ping then restart
  if 3 restarts within 3 cycles then exec /sbin/reboot
EOF
cat > /etc/monit.d/station <<EOF
check process station matching "station"
  start program = "/etc/init.d/station.sh start"
  stop program = "/etc/init.d/station.sh stop"
  if uptime > 3 days then restart
  if cpu usage > 95% for 10 cycles then restart
EOF

monit reload
/etc/init.d/station.sh start