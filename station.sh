#! /bin/bash

### BEGIN INIT INFO
# Provides:          station
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: station service
# Description:       Run Station service
### END INIT INFO

HOMEDIR=/opt/lora

case "$1" in
  start)
    echo "Starting Station..."
    $HOMEDIR/station -dfh $HOMEDIR
    ;;
  stop)
    echo "Stopping Station..."
    $HOMEDIR/station -kh $HOMEDIR
    sleep 2
    ;;
  *)
    echo "Usage: /etc/init.d/station.sh {start|stop}"
    exit 1
    ;;
esac

exit 0
